'use strict';

const _ = require("lodash");
const ConstResolver = require("./const-resolver");
const Utils = require("./utils");
const Type = require("./literal-type");

class ConstProcessor {

    /**
     *
     * @param {ParserResult} parserResult
     * @param {ErrorManager} errorManager
     * @returns {ParserResult}
     */
    static resolveConstants(parserResult, errorManager) {
        _.each(parserResult.testCases, testCase => {
            const resolver = new ConstResolver();
            const travelledConstants = [];
            //feed the resolver:

            // with global constants
            _.each(parserResult.globalContent.globalCodeblocks, codeblock => {
                _.each(codeblock.constants, constant => {
                    //noinspection JSCheckFunctionSignatures
                    resolver.add(constant.name, constant.expression.value, constant.expression.type);
                    travelledConstants.push(constant);
                });
            });

            // with test specific constants
            _.each(testCase.codeblocks, codeblock => {
                _.each(codeblock.constants, constant => {
                    //noinspection JSCheckFunctionSignatures
                    resolver.add(constant.name, constant.expression.value, constant.expression.type);
                    travelledConstants.push(constant);

                });
            });


            Utils.checkDuplicateConstants(travelledConstants, "", errorManager);

            if(errorManager.hasErrors()){
                return parserResult;
            }

            const valueReplacementAction = (parsedValueElement, searchString, newVal, type, final) => {
                if(!_.isString(parsedValueElement.value)) {
                    return;
                }
                const replacement = Utils.replaceAll(parsedValueElement.value, searchString, newVal);
                if (parsedValueElement.value !== replacement) {
                    parsedValueElement.value = replacement;
                    if (final) {
                        parsedValueElement.type = type;
                        parsedValueElement.replaced = true;
                    }
                }
            };

            const values = Utils.getValuesOfType(parserResult, testCase.title, [Type.CONST, Type.UNTYPED]);

            try {
                // now do the replacement
                resolver.replace((current, previous, type, final) => {

                    const old = ConstResolver.isConstName(current) ? current : "$" + current;
                    const newVal = final ? previous : "$" + previous;

                    const replaceSearchString = old; // TODO: support escaping like "\$ConstNotToBeReplaced"

                    _.each(values.prePostConstants, ppConstant => valueReplacementAction(ppConstant.expression, replaceSearchString, newVal, type, final));
                    _.each(values.tableValues, tableVal => valueReplacementAction(tableVal, replaceSearchString, newVal, type, final));
                });
            }catch(e){
                // ignore - checks are done later
            }
            // check if everything has been replaced:
            _.each(_.xor(values.prePostConstants, _.filter(values.prePostConstants, {expression:{replaced: true }})), unresolved => {
                if(unresolved.expression.type !== Type.UNTYPED){
                    errorManager.raise("Could not resolve pre/post constant '" + unresolved.name + "'", unresolved.expression.location, unresolved.expression.path);
                }
            });

            _.each(_.xor(values.tableValues, _.filter(values.tableValues, {'replaced': true })), unresolved => {
                if(unresolved.type !== Type.UNTYPED){
                    errorManager.raise("Could not resolve value '" + unresolved.value + "'", unresolved.location, unresolved.path);
                }
            });

            // mark unclear values
            _.eachRight(values.tableValues, cell => {
                if(!Utils.isCorrectType(cell.validType, cell.type)){
                    cell.type = Type.UNTYPED;
                }
            });

            _.eachRight(values.prePostConstants, ppConstant => {
                if(!Utils.isCorrectType(ppConstant.validType, ppConstant.expression.type)){
                    ppConstant.expression.type = Type.UNTYPED;
                }
            });
        });

        return parserResult;
    }
}

module.exports = ConstProcessor;