'use strict';
const Enum = require("enumify").Enum;

/**
 * @interface ValidType
 * @property {string} string
 * @property {string} boolean
 * @property {string} date
 * @property {string} number
 */
class ValidType extends Enum{}
ValidType.initEnum(["string", "boolean", "date", "number"]);

module.exports = ValidType;
