'use strict';

const Enum = require("enumify").Enum;
/**
 * @interface LiteralType
 * @property {string} NULL
 * @property {string} BOOL
 * @property {string} NUMBER
 * @property {string} DATE
 * @property {string} JSON
 * @property {string} STRING
 * @property {string} ML_STRING
 * @property {string} COMMENT
 * @property {string} IMPORT
 * @property {string} HEADER
 * @property {string} UNTYPED
 * @property {string} CONST
 * @readonly
 */
class LiteralType extends Enum{

    get validTypeName(){
        const typeName = this.name + "";

        if(["NULL", "NUMBER", "DATE", "STRING"].indexOf(this.name) != -1){
            return typeName.toLowerCase();
        }

        if(typeName === "BOOL"){
            return "boolean";
        }

        if(typeName === "JSON" || typeName === "ML_STRING"){
            return "string";
        }

        return "undefined";
    }
}
LiteralType.initEnum(["NULL", "BOOL", "NUMBER", "DATE", "JSON", "STRING", "ML_STRING", "COMMENT", "IMPORT", "HEADER", "UNTYPED", "CONST"]);
module.exports = LiteralType;