/**
 * Created by Florian on 26.10.2015.
 */

'use strict';

/**
 *
 * //TODO: Update
 * **********************************
 * ** DEFINITION OF PARSING RESULT **
 * **********************************
 *
 * @typedef {Object} ParserResult
 * @property {ErrorManager} linter
 * @property {Object} test
 * @property {GlobalContent} test.globalContent
 * @property {Array<TestCase>} testCases
 *
 * @typedef {Object} Location
 * @property {Object} start
 * @property {Number} start.offset
 * @property {Number} start.line
 * @property {Number} start.column
 * @property {Object} end
 * @property {Number} end.offset
 * @property {Number} end.line
 * @property {Number} end.column
 *
 * @typedef {String | Number | Date | null } Expression
 *
 * @typedef {Object} Constant
 * @property {String} name
 * @property {Expression} expression
 *
 * @typedef {Object} PrePostConstant
 * @property {String} name
 * @property {String} type
 * @property {Expression} expression
 *
 * @typedef {Object} Table
 * @property {Array<TableHeader>} header
 * @property {Array<TableContent>} content
 *
 * @typedef {Object} TableHeader
 * @property {String} title
 * @property {String} type
 *
 * @typedef {Expression} TableContent
 *
 * @typedef {Object} ParsedObject
 * @property {String | Constant | PrePostConstant | TableHeader | TableContent} value
 * @property {Location} location
 * @property {String} path
 * //TODO: ask jonas how to use generics for better type checking here
 *
 * @typedef {Object} Codeblock
 * @property {Array<ParsedObject>} constants
 * @property {Array<ParsedObject>} preConstants
 * @property {Array<ParsedObject>} postConstants
 * @property {Array<ParsedObject>} comments
 * @property {Array<ParsedObject>} imports
 *
 * @typedef {Object} TestCase
 * @property {String} title
 * @property {Array<ParsedObject>} comments
 * @property {Array<Codeblock>} codeblocks
 * @property {Table} table
 * @property {Location} location
 * @property {String} path
 *
 * @typedef {Object} LinterError
 * @property {String} message
 * @property {Location} location
 * @property {String} path
 *
 * @typedef {Object} LinterStackObject
 * @property {String} blockName
 * @property {Location} location
 *
 * @typedef {Object} Linter
 * @property {Array<LinterStackObject>} stack
 * @property {Array<LinterError>} errors
 *
 * @typedef {Object} GlobalContent
 * @property {Array<ParsedObject>} globalComments
 * @property {Array<ParsedObject>} globalCodeblocks
 * @property {Table} globalTable
 */

let singleton;
singleton = Symbol();
let singletonEnforcer = Symbol();

const PEG = require("pegjs");
const fs = require("fs");
const path = require("path");
const pegImport = require("./pegjs-simple-import");
const _ = require("lodash");
const SimpleArithmeticsResolver = require("./simple-arithmetics-resolver");
const ConstProcessor = require("./const-processor");
const WorkingDirectory = path.join(__dirname, "../");
const ErrorManager = require("./error-manager.js");
const Utils = require("./utils.js");

class MDTKitParser {


    /**
     * @param enforcer
     * @private
     */
    constructor(enforcer) {
        if(enforcer != singletonEnforcer)
            throw "Cannot construct singleton";

        this.grammar = null;
        this._currentFilePath = null;
        this._travelledGlobalImports = [];
        this._travelledLocalImports = {};
        /**
         * @type {ErrorManager}
         * @private
         */
        this._errorManager = null;
        this._parserResult = null;
        this._cleanedResult = [];

        this._generateParser();
    }

    /**
     *
     * @returns {MDTKitParser}
     */
    static get instance() {
        if(!this[singleton]) {
            this[singleton] = new MDTKitParser(singletonEnforcer);
        }
        return this[singleton];
    }

    /**
     * @private
     */
    _createGrammar(){
        if(this.grammar === null ){
            this.grammar = pegImport.GetContentWithResolvedDependencies(path.join(WorkingDirectory, "pegjs/mdtkit.pegjs"));
        }
        return this.grammar;
    }

    /**
     * @private
     */
    _generateParser(){
        this.PEGparser = this._buildParser("mdtkit.js");
        this.importParser = this._buildParser("mdtkit-import.js", "ImportGrammar");
        this.arithmeticParser = this._buildParser("mdtkit-arithmetics.js", "ValidArithmeticalResolvedStatement");
    }

    _buildParser(filename, startRule){
        const options = {output: "source"};
        if(_.isString(startRule)){
            options.allowedStartRules = [startRule];
        }
        let parserClass = null;
        const dirName = "generated_parsers";
        const requirePath = "./" + dirName + "/" + filename;
        const filePath =  path.join(WorkingDirectory, "src", dirName, filename);
        try{
            parserClass = require(requirePath);
        }catch(e){
            Utils.ensureDirectoryExistence(filePath);
            const parserCode = "module.exports = " + PEG.buildParser(this._createGrammar(), options);
            fs.writeFileSync(filePath, parserCode);
            parserClass = require(requirePath);
        }
        return parserClass;
    }

    /**
     * @returns {String}
     */
    get CurrentFilePath(){
        return this._currentFilePath;
    }

    /**
     *
     * @returns {ErrorManager}
     */
    get ErrorManager(){
        return this._errorManager;
    }

    parseText(text, currentFilePath){
        this._innerParse(text, currentFilePath);
        this._errorManager.deduplicate();
        return{
          "tests": this._cleanedResult,
            "linter":{
                "errors" : this._errorManager.errors,
                "stack" : this._errorManager.stack
            }
        };
    }

    /**

     * @param filepath
     */
    parse(filepath){
        return this.parseText(fs.readFileSync(filepath).toString(), filepath);
    }

    /**
     * @private
     */
    _innerParse(text, currentFilepath){

        // reset before every parsing action
        this._errorManager = new ErrorManager(this);
        this._currentFilePath = null;
        this._travelledGlobalImports = [];
        this._travelledLocalImports = {};
        this._parserResult = null;

        this._currentFilePath = path.resolve(currentFilepath);
        if(this.PEGparser === null) {
            this._errorManager.raise("No parser found");
            return;
        }
        try {
            this._parserResult = this.PEGparser.parse(text);
        } catch(exception){
            this._errorManager.raise(exception.message, exception.location);
        }
        if(this._errorManager.hasErrors()){
            return;
        }

        this._parseImports();
        if(this._errorManager.hasErrors()){
            return;
        }

        this._resolveGlobalTable();
        if(this._errorManager.hasErrors()){
            return;
        }

        this._resolveConstants();

        if(this._errorManager.hasErrors()){
            return;
        }

        this._resolveSimpleArithmetics();
        if(this._errorManager.hasErrors()){
            return;
        }

        this._cleanResult();

        this._checkCleanedResultForDuplicateConstants();
        if(this._errorManager.hasErrors()){
            return;
        }

        this._typeCheckCleanedResult();
    }

    /**
     * @private
     */
    _parseImports() {

        if(_.isUndefined(this.importParser) || _.isNull(this.importParser)){
            return; //TODO: error handling
        }

        let stop = false; //TODO: find more elegant break mechanism
        // first, resolve all global imports
        _.eachRight(this._parserResult.globalContent.globalCodeblocks, codeblock => {
            if(stop){
                return;
            }
            _.eachRight(codeblock.imports, imp => {
                if(stop)return;

                if(this._globalImportAlreadyVisited(imp.value)){
                    this._errorManager.raise(
                            "Circular reference with global import (" + imp.value + ")",
                            imp.location, imp.path);
                    stop = true;
                    return;
                }
                const importResult = this._parseImport(imp);

                if(this._errorManager.hasErrors()){
                    stop = true;
                    this._errorManager.raise("Parser errors in import file '" + imp.value + "'", imp.location, imp.path);
                    return;
                }
                _.remove(codeblock.imports, imp);

                this._parserResult.globalContent.globalComments = _.concat(this._parserResult.globalContent.globalComments, importResult.globalComments);
                this._parserResult.globalContent.globalCodeblocks = _.concat(this._parserResult.globalContent.globalCodeblocks, importResult.globalCodeblocks);

                // multiple global tables are not supported!
                if(this._parserResult.globalContent.globalTable !== null && importResult.globalTable !== null) {
                    // report errors about both tables
                    this._errorManager.raise(
                        "multiple global tables detected during import!",
                        this._parserResult.globalContent.globalTable.location,
                        this._parserResult.globalContent.globalTable.path);
                    this._errorManager.raise("multiple global tables detected during import!",
                        importResult.globalTable.location,
                        importResult.globalTable.path);
                    stop = true;
                }
            });
        });

        if(stop){
            return;
        }

        // recursion until all global imports are resolved.
        if(_.some(this._parserResult.globalContent.globalCodeblocks, codeblock => !_.isEmpty(codeblock.imports))){
           this._parseImports();
        }

        // then resolve all imports in test cases
        _.eachRight(this._parserResult.testCases, testCase => {
            if(stop)return;
            _.eachRight(testCase.codeblocks, codeblock => {
                if(stop)return;
                _.eachRight(codeblock.imports, imp => {
                    if(stop)return;
                    if(this._localImportAlreadyVisited(imp.value)){
                        this._errorManager.raise(
                                "Circular reference with local import '" + imp.value + "' of test '" + testCase.title + "'",
                                imp.location, imp.path);
                        stop = true;
                        return;
                    }

                    const localImportResult = this._parseImport(imp);

                    if(this._errorManager.hasErrors()){
                        stop = true;
                        return;
                    }

                    // push all global codeblocks / comments from other files into test specific ones.
                    testCase.comments = _.concat(testCase.comments, localImportResult.globalComments);
                    testCase.codeblocks = _.concat(testCase.codeblocks, localImportResult.globalCodeblocks);

                    // multiple global tables are not supported!
                    if(testCase.table !== null && localImportResult.globalTable !== null){
                        // report errors about both tables
                        this._errorManager.raise(
                            "multiple tables detected during import in '" + testCase.title + "'!",
                            testCase.table.location,
                            testCase.table.path);
                        this._errorManager.raise(
                                "multiple tables detected during import in '" + testCase.title + "'!",
                                localImportResult.globalTable.location,
                                localImportResult.globalTable.path);
                        stop = true;
                        return;
                    }

                    // push imported global table into test table if necessary
                    if(testCase.table === null){
                        testCase.table = localImportResult.globalTable;
                    }

                    _.remove(codeblock.imports, imp);
                });
            });
        });

        if(stop){
            return;
        }
        // recursion if some of the local import statements are not replaced yet
        if(_.some(this._parserResult.testCases, (testCase) => _.some(testCase.codeblocks, codeblock => ! _.isEmpty(codeblock.imports)))){
           this._parseImports();
        }
    }

    /**
     * @returns {ParserResult}
     * @private
     */
    _parseImport(importEntry){

        const importPath = path.join(path.dirname(importEntry.path), importEntry.value);
        if(Utils.fileExists(importPath)){
            this._currentFilePath = importPath;
            return this.importParser.parse(fs.readFileSync(importPath).toString());
        }

        this._errorManager.raise(
            "Cannot find file to import: '" + importPath +"'",
            importEntry.location,
            importEntry.path
        );

        return undefined;
    }

    /**
     *
     * @param importName
     * @param testName
     * @returns {boolean}
     * @private
     */
    _localImportAlreadyVisited(importName, testName){
        if(!_.has(this._travelledLocalImports, testName)){
            this._travelledLocalImports[testName] = [importName];
            return false;
        }

        if(_.indexOf(this._travelledLocalImports[testName], importName) !== -1){
            return true;
        }
        this._travelledLocalImports[testName].push(importName);
        return false;
    }

    /**
     *
     * @param importName
     * @returns {boolean}
     * @private
     */
    _globalImportAlreadyVisited(importName){
        if(_.indexOf(this._travelledGlobalImports, importName) !== -1){
            return true;
        }
        this._travelledGlobalImports.push(importName);
        return false;
    }


    /**
     * @private
     */
    _resolveSimpleArithmetics() {
        this._parserResult = new SimpleArithmeticsResolver(this.arithmeticParser).resolve(this._parserResult, this._errorManager);
    }

    /**
     * @private
     */
    _resolveGlobalTable() {

        const globalTable = this._parserResult.globalContent.globalTable;

        _.each(this._parserResult.testCases, testCase => {
            if(_.isNull(testCase.table)|| _.isUndefined(testCase.table)){

                testCase.table = globalTable;

                if(_.isNull(testCase.table)|| _.isUndefined(testCase.table)){
                    this._errorManager.raise("No table defined for test: '" + testCase.title + "'", testCase.location, testCase.path);
                }
            }
        });
    }

    /**
     * @private
     */
    _cleanResult() {
        this._cleanedResult = [];

        _.each(this._parserResult.testCases , testCase => {
            const cleanedTestCase = {title: testCase.title};
            cleanedTestCase.table = testCase.table;
            cleanedTestCase.preConstants = [];
            cleanedTestCase.postConstants = [];

            //for global constants:
            _.each(this._parserResult.globalContent.globalCodeblocks, codeblock => {
                // ... pres, ...
                _.each(codeblock.preConstants, pre => {
                    cleanedTestCase.preConstants.push(pre);
                });

                // ... posts, ...
                _.each(codeblock.postConstants, post => {
                    cleanedTestCase.postConstants.push(post);
                });
            });

            _.each(testCase.codeblocks, codeblock => {
                // ... pres, ...
                _.each(codeblock.preConstants, pre => {
                    cleanedTestCase.preConstants.push(pre);
                });

                // ... posts, ...
                _.each(codeblock.postConstants, post => {
                    cleanedTestCase.postConstants.push(post);
                });
            });

            this._cleanedResult.push(cleanedTestCase);
        });
    }

    /**
     * @private
     */
    _typeCheckCleanedResult() {

        const typeCheck = (valueArray, isPrePostArray) => {
            _.each(valueArray, elem => {

                const validType = elem.validType;
                const valueHolder = isPrePostArray ? elem.expression : elem;
                const value = isPrePostArray? elem.expression.value : elem.value;
                const type = isPrePostArray? elem.expression.type : elem.type;
                const path = isPrePostArray ? elem.expression.path : elem.path;
                const location = isPrePostArray ? elem.expression.location : elem.location;

                if(!Utils.isCorrectType(validType, type)){
                    this._errorManager.raise("Expression '" + value + "' has wrong type.\n" +
                        "Required: '" + validType + "'\t Actual: '" + type.validTypeName + "'.", location, path);
                }else{
                    // check all strings and ensure correct typing (may not be correct due to const resolving)
                    // TODO: move to better location
                    if(_.isString(valueHolder.value)){
                        const newValue = this.arithmeticParser.parse(valueHolder.value).value;
                        if(!_.isEqual(valueHolder.value, newValue)) {
                            valueHolder.value = newValue;
                            elem.arithmeticallyResolved = true;
                        }
                    }
                }
            });
        };

        _.each(this._cleanedResult, test => {
            typeCheck(_.flatten(test.table.content), false);
            typeCheck(test.preConstants, true);
            typeCheck(test.postConstants,  true);
        });
    }

    /**
     * @private
     */
    _checkCleanedResultForDuplicateConstants() {

        _.each(this._cleanedResult, testCase => {
            Utils.checkDuplicateConstants(testCase.preConstants, "pre-", this._errorManager);
            Utils.checkDuplicateConstants(testCase.postConstants, "post-", this._errorManager);
        });
    }

    /**
     * @private
     */
    _resolveConstants() {
        this._parserResult = ConstProcessor.resolveConstants(this._parserResult, this._errorManager);
    }
}

module.exports = MDTKitParser;
