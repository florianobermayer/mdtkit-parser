/**
 * Created by Florian on 27.04.2016.
 */
'use strict';

const ConstResolver = require("../src/const-resolver");
const _ = require("lodash");
const Type = require("../src/literal-type");

module.exports = {
    setUp,
    smokeTest,
    testVisualized,
    testVisualizedLargeDummyConstants,
    testConstantValueNull,
    performanceTestWithoutConstResolver,
    performanceTestWithConstResolver

};


const largeDummyDictSize = 200;
const largeDummyDictConstLength = 200;
let dummyConstants;
let stopWatch;

let constResolver;
function setUp(callback){
    constResolver = new ConstResolver();
    dummyConstants = {
        A: {value: 5, type : Type.NUMBER},
        B: {value: "$A", type: Type.CONST},
        C: {value: "a string", type: Type.STRING},
        D: {value: "$F", type: Type.CONST},
        E: {value: "$C", type: Type.CONST},
        F: {value: "$B", type: Type.CONST}
    };

    stopWatch = new StopWatch();
    callback();

}


function smokeTest(test){

    constResolver.addRange(dummyConstants);
    constResolver.replace(replaceVerbose);

    constResolver = new ConstResolver();
    constResolver.replace(replaceVerbose);

    constResolver.addRange(dummyConstants);
    constResolver.replace(replaceVerbose);

    constResolver = new ConstResolver();
    constResolver.add("Bla", "look a value!", Type.STRING);
    constResolver.add("Blubb", "$ACONSTANTASVALUE", Type.CONST);
    constResolver.add("ACONSTANTASVALUE", 42, Type.NUMBER);
    constResolver.replace(replaceVerbose);
    test.done();
}
function testVisualized(test){
    constResolver.addRange(dummyConstants);
    console.log(constResolver.toString());
    console.log("DummyConstants Dict as content to be replaced:");
    let result = JSON.stringify(dummyConstants, null, 2);

    console.log(result);

    constResolver.replace((oldVal, newVal, final) => {
        replaceVerbose(oldVal, newVal, final);
        result = replaceJSONContent(result, oldVal, newVal, final);
    });

    console.log("\nAfter replacement:");
    console.log(result);
    test.done();
}

function testVisualizedLargeDummyConstants(test){

    const largeDummyConstants = generateRandomDummyFiles(test, largeDummyDictSize, largeDummyDictConstLength);
    constResolver.addRange(largeDummyConstants);
    console.log(constResolver.toString());
    console.log("DummyConstants Dict as content to be replaced:");
    let result = JSON.stringify(largeDummyConstants, null, 2);

    console.log(result);
    try{
        constResolver.replace((oldVal, newVal, final) => {
            replaceVerbose(oldVal, newVal, final);
            result = replaceJSONContent(result, oldVal, newVal, final);
        });
    }catch(e){
        console.log(e.message)
    }

    console.log("\nAfter replacement:");
    console.log(result);
    test.done();
}

function testConstantValueNull(test){
    dummyConstants["NULLVALUE"] = {value:null, type: Type.NULL};
    constResolver.addRange(dummyConstants);
    console.log(constResolver.toString());
    constResolver.replace(replaceVerbose);
    test.done();
}

function performanceTestWithoutConstResolver(test){

    let replacementCounter = 0;
    let content = JSON.stringify(dummyConstants, null, 2);

    console.log("DummyConstants Dict as content to be replaced:");
    console.log(content);

    stopWatch.start();

    _.each(dummyConstants, () => {
        _.each(dummyConstants,  (value, key) => {
            content = replaceJSONContent(content, key, value, false);
            replacementCounter++;
        });
    });
    stopWatch.stop();

    console.log("\nAfter replacement:");
    console.log(content);

    console.log("\nStopwatch elapsed Time: " + stopWatch.elapsed);
    console.log("Stopwatch elapsed Ticks : " + stopWatch.elapsedTicks);
    console.log("Total Replacements: " + replacementCounter);


    stopWatch.reset();
    replacementCounter = 0;
    const largeDummyConstants = generateRandomDummyFiles(test, largeDummyDictSize, largeDummyDictConstLength);
    console.log("\nRepeat test with large constants dict (size: " + _.size(largeDummyConstants) + ").");

    content = JSON.stringify(largeDummyConstants, null, 2);

    stopWatch.start();

    _.each(largeDummyConstants, () => {
        _.each(largeDummyConstants,  (value, key) => {
            content = replaceJSONContent(content, key, value, false);
            replacementCounter++;
        });
    });
    stopWatch.stop();

    console.log("\nStopwatch elapsed Time: " + stopWatch.elapsed);
    console.log("Stopwatch elapsed Ticks : " + stopWatch.elapsedTicks);
    console.log("Total Replacements: " + replacementCounter);

    test.done();
}

function performanceTestWithConstResolver(test){
    let replacementCounter = 0;
    let content = JSON.stringify(dummyConstants, null, 2);

    console.log("DummyConstants Dict as content to be replaced:");
    console.log(content);

    stopWatch.start();

    constResolver.replace((oldVal, newVal, final) => {
        content = replaceJSONContent(content, oldVal, newVal, final);
        replacementCounter++;
    });
    stopWatch.stop();

    console.log("\nAfter replacement:");
    console.log(content);

    console.log("\nStopwatch elapsed Time: " + stopWatch.elapsed);
    console.log("Stopwatch elapsed Ticks : " + stopWatch.elapsedTicks);
    console.log("Total Replacements: " + replacementCounter);


    stopWatch.reset();
    replacementCounter = 0;
    const largeDummyConstants = generateRandomDummyFiles(test, largeDummyDictSize, largeDummyDictConstLength);
    console.log("\nRepeat test with large constants dict (size: " + _.size(largeDummyConstants) + ").");

    content = JSON.stringify(largeDummyConstants, null, 2);

    constResolver = new ConstResolver();
    stopWatch.start();

    constResolver.addRange(largeDummyConstants);

    try {
        constResolver.replace((oldVal, newVal, final) => {
            content = replaceJSONContent(content, oldVal, newVal, final);
            replacementCounter++;
        });
    }catch(e){
        console.log(e.message);
    }
    stopWatch.stop();

    console.log("\nStopwatch elapsed Time: " + stopWatch.elapsed);
    console.log("Stopwatch elapsed Ticks : " + stopWatch.elapsedTicks);
    console.log("Total Replacements: " + replacementCounter);

    test.done();
}

function generateRandomDummyFiles(test, dictSize, differentConstSize){
    test.ok(dictSize <= differentConstSize);

    const constNamesList = generateConstNames(differentConstSize);
    const valueConstList = _.map(constNamesList, str => str.toString());
    const possibleResults = [
        [Type.STRING, "\"a string\""],
        [Type.STRING, "'another string '"],
        [Type.NUMBER, 42],
        [Type.JSON, {}],
        [Type.DATE, new Date()],
        [Type.NUMBER, -.4],
        [Type.JSON, []],
        [Type.NULL, null],
        [Type.NULL, undefined]
    ];

    const result = {};

    for(let i = 0; i< dictSize; i++){
        const key = constNamesList[getRandomInt(constNamesList.length)];
        _.pull(constNamesList, key);

        const value = {type : Type.CONST, value: undefined};

        do{
            value.value = "$" + valueConstList[getRandomInt(valueConstList.length)];
        }while (("" + value) === key);

        if(Math.random() > 0.5){
            const rand = getRandomInt(possibleResults.length);
            value.type = possibleResults[rand][0];
            value.value = possibleResults[rand][1];
        }
        result[key] = value;
    }
    return result;
}

function generateConstNames(count) {
    const result = [];
    const chars = "0ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');

    let counter = 0;
    let i = 0;
    while(counter < count) {
        let tmp = intToString(i, chars);


        if(tmp.indexOf("0") === -1) {
            result.push(tmp);
            counter++;
        }
        i++;
    }

    return result;
}

function intToString(value, baseChars)
{
    let result = "";
    const targetBase = baseChars.length;

    do {
        result = baseChars[value%targetBase] + result;
        value = Math.floor(value/targetBase);
    } while(value > 0);

    return result;
}

function randomString(size) {
    let builder = "";
    let ch;
    for(let i = 0; i < size; i++) {
        ch = String.fromCharCode(Math.floor(26*Math.random()+ 65));
        builder += ch;
    }

    return builder;
}


function replaceVerbose(oldValue, newValue, type, finalReplacement) {
    console.log("Replacing '" + oldValue + "' with '" + newValue + "' (" + type + ")" + (finalReplacement ? " <- final" : ""));
}

function replaceJSONContent(content, oldValue, newValue, final) {

    const oldVal = ConstResolver.isConstName(oldValue) ? oldValue : "$" + oldValue;
    const newVal = final ? newValue : "$" + newValue;
    const regex = new RegExp("\"\\" + oldVal + "\"", 'gm');

    if(final && !_.isString(newVal)) {
        content = _.replace(content, regex, newVal.toString());
    } else {
        try{
            content = _.replace(content, regex , "\"" + newVal + "\"");
        }catch(e){
            // ignore.. a bug in string replace
        }
    }
    return content;
}


function getRandomInt(max) {
    return Math.floor(Math.random() * max) ;
}




class StopWatch{

    constructor(){
        this.startTime = null;
        this.endTime = null;
    }

    start(){
        this.startTime =  new Date();
    }

    stop(){
        this.endTime =  new Date();
    }

    reset(){
        this.startTime = null;
        this.endTime = null;
    }

    get elapsed(){
        return new Date(this.elapsedTicks).toISOString().substr(11,12);
    }

    get elapsedTicks(){
        return this.endTime - this.startTime;
    }
}