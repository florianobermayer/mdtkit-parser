This file contains a test to check if the ConstResolver of the MDTKit works as expected.

ConstantResolving
-----------------

This test tests if constants are resolved properly. Therefore, const values are created and then saved as pre constants. These pre constants have name-matching post constants.

The test should check for every pre constant: pre[X] == post[X]

```
const A = 5;
const B = $A;
const C = "a string";
const D = $F;
const E = $C;
const F = $B;

```

```
pre A:number = $A;
post A:number = 5;

pre B:number = $B;
post B:number = 5;

pre C:string = $C;
post C:string = "a string";

pre D:number = $D;
post D:number = 5;

pre E:string = $E;
post E:string = "a string";

pre F:number = $F;
post F:number = 5;

```

| empty:boolean |
|:--------------|
| false         |
