ConstantResolvingFail
---------------------

const UNUSED = $IIIUNUSED;

const IUNUSED = $UNUSED;

const IIUNUSED = $IUNUSED;

const IIIUNUSED = $IIUNUSED;

const IIIIUNUSED = $IIIUNUSED;

const IIIIIUNUSED = $IIIIUNUSED;

It expects a `MDParserException("Entry with Key:'IIIUNUSED' and Value:'$IIUNUSED' can't be resolved")` Exception.

| teststring:string | otherString:string |
|:------------------|:------------------:|
| "test string"     |    $IIIIIUNUSED    |
| "asdf"            |      $UNUSED       |
