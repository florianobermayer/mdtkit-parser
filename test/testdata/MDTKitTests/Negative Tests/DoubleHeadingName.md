DoubleHeadingNameTest
---------------------

This test tests the behavior of double heading names in data tables:

It expects a `MDParserException("heading 'double_heading_name' previously declared")` Exception.

| double_heading_name:number | double_heading_name:boolean |
|:--------------------------:|:---------------------------:|
|             0              |            false            |
